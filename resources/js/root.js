"use strict";

window.addEventListener('load', loadHandler);

function getTweet(i) {
    return document.querySelector('#tweets > button:nth-child('+(i.toString())+')');
}

function destroyTweet(button) {
    button.innerHTML = "";
}

function loadHandler() {
    let deathToll = 0;
    for (let i=1; i < 5 ; i++){
        getTweet(i).addEventListener('click', () => {
            destroyTweet(getTweet(i));
            deathToll = deathToll + 1;
            if (deathToll == 4){
                location.reload(true);
            }
        });
    }
}