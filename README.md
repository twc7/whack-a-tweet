This is a project for a Web Programming final. It is a website called Whack-A-Tweet. This project integrates Twitter API to have you play a
game of whack-a-mole with tweets. First it has you log into your Twitter account and then it pulls tweets with the #CSC365 and you are able to 
essentially whack the tweets away. After all the tweets are destroyed it will automatically repopulate the board the latest tweets.

Future additions could include: 
	* Tweets not be reused. As it gets boring simply destroying the same 4 tweets continuously. 
	* Hammer cursor with animation that smites down those dastardly tweets.