"use strict";

const express = require('express'),
    hoffman = require('hoffman'),
    path = require('path'),
    passport = require('passport'),
    TwitterStrategy = require('passport-twitter').Strategy,
    bodyParser = require('body-parser'),
    authConfig = require('./modules/authConfig'),
    authHandling = require('./modules/authHandling'),
    https = require('https');

const app = express();
//view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'dust');
app.engine('dust', hoffman.__express());

const listeningPort = parseInt(process.argv[2]) || 3000;
app.use('/resources', express.static('resources'));

//body parser
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

//express-session setup for cookies
app.use(require('express-session')({
    secret: 'sooooper secretive secret of secrecy',
    resave: true,
    saveUninitialized: true
}));

//passport authentication
app.use(passport.initialize());
app.use(passport.session());

//twitter authentication
let twitterOptions = {
    consumerKey: authConfig.TWITTER_CONSUMER_KEY,
    consumerSecret: authConfig.TWITTER_CONSUMER_SECRET,
    callbackURL: "http://127.0.0.1:3000/auth/twitter/callback"
};

let twitterAuthStrat = 'twitter-auth';
passport.use(twitterAuthStrat, new TwitterStrategy(twitterOptions,
    (token, tokenSecret, profile, done) => {
        done(null, {
            profile: profile,
            token: token,
            tokenSecret: tokenSecret
        });
    }
));

//Session persistence
passport.serializeUser(function (user, cb) {
    console.log("Serializing user");
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    console.log("Serializing user");
    cb(null, obj);
});


app.get('/', (request, response) => {
    response.render('index', {});
});

// Redirect to Twitter for authentication.
app.get('/auth/twitter', passport.authenticate(twitterAuthStrat));

app.get('/auth/twitter/callback', passport.authenticate(twitterAuthStrat, {successRedirect: '/', failureRedirect: '/'}));

//Protected page only accessible when logged in
app.get('/protected',
    (request, response) => {
        if (request.user) { //Check login
            console.log(request.user);
            response.render('protected', request.user._json);
        } else {
            response.redirect('/auth/twitter');//redirect to login if they arent logged in
        }
    }
);

app.get('/auth/logout', (request, response) => {
    request.logout();
    response.redirect('/');
});

let searchOptions = {
    hostname: 'api.twitter.com',
    port: 443,
    path: '/1.1/search/tweets.json',
    method: 'GET',
};

app.get('/hashtag/:whatevs', (request, response) => {
    let user = request.user;

    if (user) { //check login
        let hashTag = request.params.whatevs;
        
        let searchSpecificOptions = Object.assign(searchOptions, {});

        searchSpecificOptions.path += `?q=${hashTag}`+ '&count=4';//Add search parameter and count number of tweets 
        
        searchSpecificOptions['headers'] = authHandling.getOauthHeader(authConfig.TWITTER_CONSUMER_KEY,
            authConfig.TWITTER_CONSUMER_SECRET,
            user.token,
            user.tokenSecret,
            hashTag); //formatted Oauth 1.0 authorization header

        //HTTP request to Twitter
        let requestToTwitter = https.request(searchSpecificOptions, (responseFromTwitter) => {
            let allTweets;
            responseFromTwitter.on('data', (tweets) => {
                if (allTweets) {
                    allTweets += tweets;
                } else {
                    allTweets = tweets;
                }
            });
            responseFromTwitter.on('error', (err) => {
                console.error(err);
            });
            responseFromTwitter.on('end', () => {
                let parsedTweets = JSON.parse(allTweets.toString());
                response.render('hashtag', parsedTweets);
                console.log(parsedTweets);
            });
        });
        requestToTwitter.end(); //end the request
    
    } else { 
        response.redirect('/auth/twitter');
    }
});

app.listen(listeningPort, () => {
    console.log(`My app is listening on port ${listeningPort}!`);
});

